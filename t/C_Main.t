use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

plan(tests => 3);

my $t;
$t = Test::Mojo->new('Demo::Mojo::OpenAPI::Server');

$t->get_ok('/api/v1/file/foo/bar.txt?debug=1')
    ->status_is(200)
    ->content_type_like(qr/application\/json/i);


