package Demo::Mojo::OpenAPI::Server::Controller::Main;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use Data::Printer;
use Mojo::URL;

sub get_status ($self) {

  $self->render(json => {
    status => 'ok',
  });

}


sub get_file_info ($self) {
  my $file;

  my $req_info = {};

  my $f = $self->param('file');
  $self->log->debug("get_file_info: f=" . ( $f // '' ));
  $req_info->{'param.file'} = ( $f // '' );

  #$self->log->debug("get_file_info: file=$file");
  $self->log->debug("get_file_info: url=" . $self->req->url);
  $req_info->{'req.url'} = $self->req->url;

  my $oa = $self->openapi;
  $self->log->debug("get_file_info: oa=" . $oa);
  $req_info->{'self.openapi.ref'} = ref $oa;

  my $spec_servers = $oa->spec('/servers');
  #$self->log->debug("get_file_info: spec_servers:\n" . np($spec_servers));

  my $spec_paths = $oa->spec('/paths');
  #$self->log->debug("get_file_info: spec_paths:\n" . np($spec_paths));

  my $this_path;
  my $this_path_spec;
  foreach my $key ( keys %{ $spec_paths } ) {
      my $path_spec = $spec_paths->{$key};
      if ( 'getFileInfo' eq $path_spec->{get}->{operationId} ) {
        $this_path = $key;
        $this_path_spec = $path_spec;
        $req_info->{'api.path'} = $key;
      }

  }
#  $self->log->debug("get_file_info: this_path_spec:\n" . np($this_path_spec));
  $self->log->debug("get_file_info: this_path=" . $this_path);

  my $path_prefix = $this_path;
  $path_prefix =~ s/\{.*$//;
  $self->log->debug("get_file_info: path_prefix=" . $path_prefix);
  $req_info->{'api.path_prefix'} = $path_prefix;

  my $server = $spec_servers->[0];
  $self->log->debug("get_file_info: server:\n" . np($server));

  my $api_url_base = $server->{url};
  $self->log->debug("get_file_info: api_url_base=" . $api_url_base);
  $req_info->{'api.url.base'} = $api_url_base;

  my $u = Mojo::URL->new($self->req->url);
  my $p = $u->path;
  $self->log->debug("get_file_info: p=" . $p);
  $file = $p;
  $file =~ s/^$api_url_base//;
  $self->log->debug("get_file_info: 1 file=" . $file);
  $file =~ s/^$path_prefix//;
  $self->log->debug("get_file_info: 2 file=" . $file);
  $req_info->{'file.calc'} = $file;

  $self->log->debug("get_file_info: req_info:\n" . np($req_info));

  $self->render(json => {
    file => $file,
    size => 1930,
  });

}


1;
