package Demo::Mojo::OpenAPI::Server;
use Mojo::Base 'Mojolicious', -signatures;

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  $self->plugin(
    OpenAPI => {
      #url => $self->home->rel_file("public/openapi.json"),
      url => $self->home->rel_file("public/openapi.yml"),
    },
  );

  $self->plugin(
    SwaggerUI => {
      route => $self->routes->any('api'),
      title => 'Demo Mojo OpenAPI Server',
      url   => '/api/v1',
    },
  );

  # Configure the application
  $self->secrets($config->{secrets});

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('Example#welcome');
}

1;
